package com.cesar.retomdp.repositories

import com.cesar.retomdp.model.User
import com.cesar.retomdp.source.RestApi
import retrofit2.Response
import java.io.IOException

class UserRepository(var api: RestApi) {

   suspend fun getListUsers(): State {
       val result: Result<*> = safeApiResult({ api.getUsers().await() as Response<*> }, "")


       when (result) {
           is Result.Success -> {
              var data = result.data
               return State.Success(data as List<User>)
           }

           is Result.Error -> {
                return State.Failure("Error")
           }
       }
    }

    sealed class State {
        data class Success(val list: List<User>) : State()
        data class Failure(val errorMessage: String) : State()
    }


    private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>, errorMessage: String): Result<T> {
        val response = call.invoke()
        if (response.isSuccessful) return Result.Success(response.body()!!)
        return Result.Error(IOException("Error - $errorMessage"))
    }

    sealed class Result<out T: Any> {
        data class Success<out T : Any>(val data: T) : Result<T>()
        data class Error(val exception: Exception) : Result<Nothing>()
    }

}