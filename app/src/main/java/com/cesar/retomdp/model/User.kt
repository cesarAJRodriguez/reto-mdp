package com.cesar.retomdp.model

import android.os.Parcelable
import java.io.Serializable

class User (
        val id : Int,
        val name : String,
        val username : String,
        val email : String,
        val address : Address,
        val phone : String,
        val website : String,
        val company : Company,
        ) : Serializable{
}

class Company (
        val name : String,
        val catchPhrase : String,
        val bs : String
) : Serializable{
}

class Address (
        val street : String,
        val suite : String,
        val city : String,
        val zipcode : String,
        val geo : Coordinate
) : Serializable{
}

class Coordinate (
        val lat : String,
        val lng : String
) : Serializable{
}
