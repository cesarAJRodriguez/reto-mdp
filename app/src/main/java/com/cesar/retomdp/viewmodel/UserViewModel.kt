package com.cesar.retomdp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.cesar.retomdp.model.User
import com.cesar.retomdp.repositories.UserRepository
import com.cesar.retomdp.source.HelperService
import kotlinx.coroutines.Dispatchers

class UserViewModel : ViewModel(){

    private val repository  = UserRepository(HelperService.getConfiguration())

    fun getUsers() = liveData(Dispatchers.IO) {
        val listUsers = repository.getListUsers() as UserRepository.State
        when(listUsers){
            is UserRepository.State.Success ->{
                 emit(ViewState.GetUsersSuccess(listUsers.list))
             }
            is UserRepository.State.Failure ->{
                 emit(ViewState.GetUsersFailure(listUsers.errorMessage))
            }
        }
    }

    sealed class ViewState() {
        data class GetUsersSuccess(val list: List<User>) : ViewState()
        data class GetUsersFailure(val error: String) : ViewState()
    }

}