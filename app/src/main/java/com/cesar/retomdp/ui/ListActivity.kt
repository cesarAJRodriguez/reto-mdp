package com.cesar.retomdp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.cesar.retomdp.adapter.ItemListUserAdapter
import com.cesar.retomdp.databinding.ActivityListBinding
import com.cesar.retomdp.model.User
import com.cesar.retomdp.viewmodel.UserViewModel
import kotlinx.coroutines.Dispatchers

class ListActivity : AppCompatActivity() {

    private lateinit var viewmodel: UserViewModel
    private  lateinit var binding : ActivityListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViewModel()
        setupData()
    }

    private fun initViewModel() {
        viewmodel = ViewModelProvider(this).get(UserViewModel::class.java)
    }



    private fun setupData() {
        binding.toolbar.title = "Usuarios"

        viewmodel.getUsers().observe(this, Observer {
            when(it){
                is UserViewModel.ViewState.GetUsersSuccess ->{

                    val listener = object : ItemListUserAdapter.ItemListUserAdapterListener{
                        override fun onclickItem(user: User) {
                            val intent = Intent(this@ListActivity, DetailActivity::class.java).apply {
                                putExtra("USER", user)
                            }
                            startActivity(intent)
                        }
                    }

                    binding.rvList.layoutManager = LinearLayoutManager(this@ListActivity)
                    val adapter =  ItemListUserAdapter(listener,it.list)
                    binding.rvList.adapter =  adapter
                }
                is UserViewModel.ViewState.GetUsersFailure ->{
                    Toast.makeText(this@ListActivity, it.error, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }


}