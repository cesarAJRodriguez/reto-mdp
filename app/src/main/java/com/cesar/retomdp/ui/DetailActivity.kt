package com.cesar.retomdp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cesar.retomdp.R
import com.cesar.retomdp.databinding.ActivityDetailBinding
import com.cesar.retomdp.model.User
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class DetailActivity : AppCompatActivity() , OnMapReadyCallback {

    private  lateinit var binding : ActivityDetailBinding
    private var user : User?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment =  supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        setupData()

    }

    private fun setupData() {
        user = intent.getSerializableExtra("USER") as User?

        user?.let {
            binding.toolbar.title = it.name
            binding.tvNickName.text = it.username
            binding.tvEmail.text = it.email
            binding.tvPhone.text = it.phone
            binding.tvWeb.text = it.website
            binding.tvCompany.text = it.company.name
            binding.tvAddress.text = "${it.address.street} ${it.address.suite} ${it.address.city}"

        }


    }

    override fun onMapReady(map: GoogleMap?) {
          user?.let {
              val coordinate = LatLng((it.address.geo.lat).toDouble(),(it.address.geo.lng).toDouble())
              map?.addMarker(MarkerOptions().position(coordinate))
              map?.animateCamera(CameraUpdateFactory.newLatLng(coordinate))
          }
    }


}