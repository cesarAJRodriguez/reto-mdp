package com.cesar.retomdp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.cesar.retomdp.R
import com.cesar.retomdp.model.User
import java.util.ArrayList

class ItemListUserAdapter(val listener: ItemListUserAdapterListener,val items: List<User>) : RecyclerView.Adapter<ItemListUserAdapter.ItemListUserAdapterViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemListUserAdapterViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ItemListUserAdapterViewHolder(v)
    }

    override fun onBindViewHolder(holder: ItemListUserAdapterViewHolder, position: Int) {
       val item =  items[position]
        holder.tvName?.text = item.name
        holder.tvPhone?.text = item.email

        holder.container?.setOnClickListener {
            listener.onclickItem(item)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ItemListUserAdapterViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
         var tvName = itemView?.findViewById<TextView>(R.id.tvName)
         var tvPhone = itemView?.findViewById<TextView>(R.id.tvPhone)
         var container = itemView?.findViewById<ConstraintLayout>(R.id.container)
    }

    interface ItemListUserAdapterListener {
        fun onclickItem(user: User)
    }

}