package com.cesar.retomdp.source

import com.cesar.retomdp.model.User
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface RestApi {

    @GET("/users")
    fun getUsers(): Deferred<Response<List<User>>>
}